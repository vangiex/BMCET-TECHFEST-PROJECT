<!-- 
/* 
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
-->
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="">
        <!-- Meta Description -->
        <meta name="description" content="AAYAM IS THE OFFICAL TECHFEST OF BMEF SURAT GUJARAT INDIA">
        <!-- Meta Keyword -->
        <meta name="keywords" content="BMEF TECHFEST">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>AAYAM | BMEF</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
        <link rel="stylesheet" href="css/linearicons.css">=
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/main.js"></script>
        <script type="application/x-javascript"> 
            addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
        </script>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="//fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">-->
        <link href="//fonts.googleapis.com/css?family=Signika:300,400,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    </head>
    <body>
        <!-- Start Header Area -->
        <header class="default-header">
            <div class="container">
                <div class="header-wrap">
                    <div class="header-top d-flex justify-content-between align-items-center">
                        <div class="logo">
                            <a href="enroll.php"><img src="img/logo.png" alt=""></a>
                        </div>
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide">
                                <a href="index.php#home">Home</a>
                                <a href="index.php#aayam">AAYAM</a>
                                <a href="index.php#events">EVENTS</a>
                                <a href="enrollment.php">Enrollments</a>
                                <a href="enroll.php">Particepate</a>
                                <a href="index.php#about">ABOUT US</a>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section style="margin-top: 2%;">
            <div class="main-grid">
                <div class="agile-grids">	
                    <!-- validation -->
                    <div class="grids">
                        <div class="forms-grids">
                            <div class="forms3">
                                <div class="w3agile-validation w3ls-validation">
                                    <div class="panel panel-widget agile-validation register-form">
                                        <div class="validation-grids widget-shadow" data-example-id="basic-forms"> 
                                            <div class="input-info">
                                                <h3>AAYAM | 2018</h3>
                                            </div>
                                            <div class="form-body form-body-info">
                                                <form id="myform" data-toggle="validator" action="register.php" method="post">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" class="form-control" name="email" placeholder="Email" data-error="That email address is invalid" required="">
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <span class="help-block with-errors">Please enter a valid email address</span>
                                                    </div>
                                                    <div class="form-group valid-form">
                                                        <input type="text" class="form-control" id="inputName" name="college" placeholder="College" required="">
                                                    </div>
                                                    <div class="form-group valid-form">
                                                        <input type="text" class="form-control" id="inputName" name="phoneno" placeholder="Contact No." required="">
                                                    </div>
                                                    <div class="form-group valid-form">
                                                        <input type="text" class="form-control" id="inputName" name="name1" placeholder="Participant 1" required="">
                                                    </div>
                                                    <div class="form-group valid-form">
                                                        <input type="text" class="form-control" id="inputName" name="name2" placeholder="Participant 2 (optional)">
                                                    </div>
                                                    <div class="form-group valid-form">
                                                        <input type="text" class="form-control" id="inputName" name="name3" placeholder="Participant 3 (optional)">
                                                    </div>
                                                    <div class="form-group">
                                                        <select autocomplete="off" required="" name="event" class="form-control">
                                                            <option <?php if ( $_GET["set"] =="Events" ) { echo "selected " ; } ?>value="">Events</option>

                                                            <option <?php if ( $_GET["set"] =="Switch Maze" ) { echo "selected " ; } ?>value="Switch Maze">Switch Maze | Rs.100</option>

                                                            <option <?php if ( $_GET["set"] =="Logo Designing" ) { echo "selected " ; } ?>value="Logo Designing">Logo Designing | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Dam O Mania" ) { echo "selected " ; } ?>value="Dam O Mania">Dam O Mania | Rs.100</option>
 
                                                            <option <?php if ( $_GET["set"] =="NFS GAMING" ) { echo "selected " ; } ?>value="NFS GAMING">NFS GAMING | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Pirate BATTLE" ) { echo "selected " ; } ?>value="Pirate BATTLE">Pirate BATTLE | Rs.60</option>

                                                            <option <?php if ( $_GET["set"] =="Counter Strike GAMING" ) { echo "selected " ; } ?>value="Counter Strike GAMING">Counter Strike GAMING | Rs.120</option>

                                                            <option <?php if ( $_GET["set"] =="ROBO Race" ) { echo "selected " ; } ?>value="ROBO Race">ROBO Race | Rs.200</option>

                                                            <option <?php if ( $_GET["set"] =="Mini Militia" ) { echo "selected " ; } ?>value="Mini Militia">Mini Militia | Rs.60</option>

                                                            <option <?php if ( $_GET["set"] =="Short Film" ) { echo "selected " ; } ?>value="Short Film">Short Film | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Model Presentation" ) { echo "selected " ; } ?>value="Model Presentation">Model Presentation | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Town O Planning" ) { echo "selected " ; } ?>value="Town O Planning">Town O Planning | Rs.120</option>

                                                            <option <?php if ( $_GET["set"] =="Bascule Bridge" ) { echo "selected " ; } ?>value="Bascule Bridge">Bascule Bridge | Rs.80</option>

                                                            <option <?php if ( $_GET["set"] =="ROBO Soccer" ) { echo "selected " ; } ?>value="ROBO Soccer">ROBO Soccer | Rs.200</option>

                                                            <option <?php if ( $_GET["set"] =="Relay Coding" ) { echo "selected " ; } ?>value="Relay Coding">Relay Coding | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="ROBO Tug Of War" ) { echo "selected " ; } ?>value="ROBO Tug Of War">ROBO Tug Of War | Rs.150</option>

                                                            <option <?php if ( $_GET["set"] =="Cade Zap" ) { echo "selected " ; } ?>value="Cade Zap">Cade Zap | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Code War" ) { echo "selected " ; } ?>value="Code War">Code War | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Auto Sketching" ) { echo "selected " ; } ?>value="Auto Sketching">Auto Sketching | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Circuitronix" ) { echo "selected " ; } ?>value="Circuitronix">Circuitronix | Rs.50</option>

                                                            <option <?php if ( $_GET["set"] =="Techno Treasure Hunt" ) { echo "selected " ; } ?>value="Techno Treasure Hunt">Techno Treasure Hunt | Rs.80</option>

                                                            <option <?php if ( $_GET["set"] =="Eurepa" ) { echo "selected " ; } ?>value="Eurepa">Eurepa | Rs.200</option>
                                                            
                                                            <option <?php if ( $_GET["set"] =="Poster Presentation" ) { echo "selected " ; } ?>value="Poster Presentation">Poster Presentation | Rs.50</option>

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary disabled">Submit</button>
                                                    </div>
                                                </form>
                                                <form action="register.php">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary active">Or Check Enrollments</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    <script type="text/javascript" src="js/valida.2.1.6.min.js"></script>
    <script type="text/javascript" >

        $(document).ready(function () {

            // show Valida's version.
            $('#version').valida('version');
            // Exemple 1
            $('.valida').valida();
            // Exemple 2
            /*
             $('.valida').valida({
             
             // basic settings
             validate: 'novalidate',
             autocomplete: 'off',
             tag: 'p',
             
             // default messages
             messages: {
             submit: 'Wait ...',
             required: 'Este campo é obrigatório',
             invalid: 'Field with invalid data',
             textarea_help: 'Digitados <span class="at-counter">{0}</span> de {1}'
             },
             
             // filters & callbacks
             use_filter: true,
             
             // a callback function that will be called right before valida runs.
             // it must return a boolan value (true for good results and false for errors)
             before_validate: null,
             
             // a callback function that will be called right after valida runs.
             // it must return a boolan value (true for good results and false for errors)
             after_validate: null
             
             });
             */

            // setup the partial validation
            $('#partial-1').on('click', function (ev) {
                ev.preventDefault();
                $('#res-1').click(); // clear form error msgs
                $('form').valida('partial', '#field-1'); // validate only field-1
                $('form').valida('partial', '#field-1-3'); // validate only field-1-3
            });
        });
    </script>
    <script src="js/validator.min.js"></script>
   
</body>
<?php
 require 'log.php';
$status = $_GET['status'];
if ($status == 'fail')
    echo "<script type='text/javascript'>
                alert('FAILED !! INVAILD INPUT OR YOU ALERADY REGISTERED FOR THIS EVENT');
             </script>";
if ($status == 'success')
    echo "<script type='text/javascript'>
                alert('Thank You For Registeration');
             </script>";
?>
</html>

