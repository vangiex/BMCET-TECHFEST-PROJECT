<!-- 
/* 
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
-->
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="VanGiex">
        <!-- Meta Description -->
        <meta name="description" content="AAYAM IS THE OFFICAL TECHFEST OF BMEF SURAT GUJARAT INDIA">
        <!-- Meta Keyword -->
        <meta name="keywords" content="BMEF TECHFEST">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>AAYAM | BMEF</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
        <link rel="stylesheet" href="css/linearicons.css">=
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="//fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">-->
        <link href="//fonts.googleapis.com/css?family=Signika:300,400,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    </head>
    <body>
        <!-- Start Header Area -->
        <header class="default-header">
            
            <div class="container">
                <div class="header-wrap">
                    
                    <div class="header-top d-flex justify-content-between align-items-center">
                        
                        <div class="logo">
                            <a href="enrollment.php"><img src="img/logo.png" alt=""></a>
                        </div>
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide">
                                <a href="index.php#home">Home</a>
                                <a href="index.php#aayam">AAYAM</a>
                                <a href="index.php#events">EVENTS</a>
                                <a href="enrollment.php">Enrollments</a>
                                <a href="enroll.php">Particepate</a>
                                <a href="index.php#about">ABOUT US</a>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </header>
        
        <section style="margin-top: 2%;">
            <div class="main-grid">
                <div class="agile-grids">
                    <div class="grids">
                        
                        <div class="forms-grids">
                            <div class="forms3">
                                <div class="w3agile-validation w3ls-validation">
                                    <div class="panel panel-widget agile-validation register-form">
                                        
                                        <div class="validation-grids widget-shadow" data-example-id="basic-forms"> 
                                            
                                            <div class="input-info">
                                                <h3>AAYAM | 2018</h3>
                                            </div>
                                            
                                            <div class="form-body form-body-info">
                                                <form>
                                                    
                                                    <div class="form-group">
                                                        <input type="text" name="phoneno" value="" placeholder="Phone No" maxlength="10" class="form-control" id="ID">
                                                    </div>
                                                    
                                                    <button  type="submit" class="btn btn-login btn-dark">Find Me!</button>
                                                </form>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <br><br>
                            
                            <div class ="panel panel-widget " style = "color: #ffffff">
                                
                                <?php
                                 require 'log.php';
                                $phn = $_GET['phoneno'];
                                if ($phn != '' && !preg_match('/^[6-9][0-9]{9}$/', $phnno)) 
                                    {
                                        echo ' 
                                                <table class = "table" style = "color: #fff">
                                                <thead class = "thead-dark">
                                                <tr>
                                                    <th scope = "col">Event</th>
                                                    <th scope = "col">Date</th>
                                                    <th scope = "col">Venue</th>
                                                    <th scope = "col">Level</th>
                                                    <th scope = "col">Result</th>
                                                    <th scope = "col">Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                            ';

                                        require '../Database/DB_Config.php';
                                        $phn = $_GET['phoneno'];
                                        $sql = "SELECT * FROM aayam WHERE phnno = $phn ;";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows > 0) {
                                            while ($row = $result->fetch_assoc()) {
                                                echo '<tr style = "color: #fff" ><th scope = "row">' . $row["EVENT"] . '</th><td>' . $row["TIME"] . '</td><td>' . $row["VENUE"] . '</td><td>' . $row["LEVEL"] . '</td><td>' . $row["RESULT"] . '</td><td>' . $row["STATUS"] . '</td></tr>';
                                            }
                                        }
                                        echo '
                                                </tbody>
                                                </table> 
                                            ';
                                        $conn->close();
                                    }
                                ?>
                            </div>

                        </div>
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/parallax.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/main.js"></script>
<script type="application/x-javascript"> 
    addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
</script>
</body>
</html>

